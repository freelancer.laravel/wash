<?php
declare(strict_types=1);

/**
 * File for custom exception messages.
 */
return [
    'users' => [
        'deviceNameMaxLength' => 'Max length for device name is :length.',
        'phoneMaxLength' => 'Max length for phone is :length.',
        'phoneMixLength' => 'Min length for phone is :length.',
        'userExist' => 'User is exist.'
    ],
];
