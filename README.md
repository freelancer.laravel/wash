# How to up project on Linux.

## Requirements.
    - Docker
    - Docker-compose
## Setup.
    - Clone project.
    - Run this commands.
<pre>
cp .env.example .env
</pre>
<pre>
docker run --rm --interactive --tty \
  --volume $PWD:/app \
  --user $(id -u):$(id -g) \
  composer install
</pre>
<pre>
./sail up -d
./sail migrate --seed
</pre>
## Done.


# Help commands

 - Up project. `./sail up -d`
 - Down project. `./sail down`
 - Bash app. `./sail exec samwash.app bash`
 - Bash mysql. `./sail exec mysql bash`
 - Mysql shell. `./sail exec mysql mysql -uroot -proot`
 - Refresh db. `./sail artisan migrate:refresh --seed`
