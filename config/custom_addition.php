<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings Domains
    |--------------------------------------------------------------------------
    |
    | @param string client domain
    | @param string server domain
    |
    */

    'domains' => [
        'client' => env('CLIENT_DOMAIN', 'http://localhost'),
        'server' => env('SERVER_DOMAIN', 'http://localhost'),
    ]

];
