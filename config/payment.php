<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Settings Liqpay
    |--------------------------------------------------------------------------
    |
    | @param string $public_key
    | @param string $private_key
    | @param string $api_url (optional)
    |
    */

    'settings' => [
        'public_key' => env('LIQPAY_PUBLIC_KEY'),
        'private_key' => env('LIQPAY_PRIVATE_KEY'),
        'api_url' => env('LIQPAY_API_URL'),
    ]

];
