<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Orders\Order;
use Illuminate\Database\Seeder;

/**
 * Class OrderSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Order::factory(10)->create();
    }
}
