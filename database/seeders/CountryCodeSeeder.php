<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\CountryCode;
use Illuminate\Database\Seeder;

/**
 * Class CountryCodeSeeder.
 *
 * @package Database\Seeders
 * @author DaKoshin.
 */
final class CountryCodeSeeder extends Seeder
{
    /**
     * Run seeder.
     */
    public function run()
    {
        CountryCode::create(['code' => 380]);
    }
}
