<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\PaymentTargets\PaymentTarget;
use Illuminate\Database\Seeder;

/**
 * Class PaymentTargetSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentTargetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        PaymentTarget::factory()->create(['name' => 'box']);
        PaymentTarget::factory()->create(['name' => 'coin']);
        PaymentTarget::factory()->create(['name' => 'loyalty-card']);
    }
}
