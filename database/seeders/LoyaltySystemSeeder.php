<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\LoyaltySystems\LoyaltySystem;
use Illuminate\Database\Seeder;

/**
 * Class LoyaltySystemSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoyaltySystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        LoyaltySystem::factory(5)->create();
    }
}
