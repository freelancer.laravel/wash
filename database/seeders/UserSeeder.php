<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class UserSeeder.
 *
 * @package Database\Seeders
 * @author DaKoshin.
 */
final class UserSeeder extends Seeder
{
    /**
     * Run seeder.
     */
    public function run()
    {
        DB::transaction(function () {
            User::factory(10)->create();
        });
    }
}
