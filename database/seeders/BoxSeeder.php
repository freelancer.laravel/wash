<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Washes\Box;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class BoxSeeder.
 *
 * @package Database\Seeders
 * @author DaKoshin.
 */
final class BoxSeeder extends Seeder
{
    /**
     * Run seeder.
     */
    public function run()
    {
        DB::transaction(function () {
            Box::factory()->count(10)->create();
        });
    }
}
