<?php
declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder.
 *
 * @package Database\Seeders
 * @author DaKoshin.
 */
final class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(CountryCodeSeeder::class);
        $this->call(LoyaltySystemSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(LoyaltyCardSeeder::class);
        $this->call(WashSeeder::class);
        $this->call(BoxSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(PaymentSourceSeeder::class);
        $this->call(PaymentTargetSeeder::class);
        $this->call(PaymentStatusSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(ModbusSeeder::class);
    }
}
