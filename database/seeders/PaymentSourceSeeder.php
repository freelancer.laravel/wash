<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\PaymentSources\PaymentSource;
use Illuminate\Database\Seeder;

/**
 * Class PaymentSourceSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentSourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        PaymentSource::factory()->create(['name' => 'cash-register']);
        PaymentSource::factory()->create(['name' => 'terminal']);
        PaymentSource::factory()->create(['name' => 'qr-code']);
        PaymentSource::factory()->create(['name' => 'website']);
    }
}
