<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\PaymentStatuses\PaymentStatus;
use Illuminate\Database\Seeder;

/**
 * Class PaymentStatusSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentStatusSeeder extends Seeder
{
    /**
     * @var array|array[] Payment Statuses
     */
    private array $paymentStatuses = [
        //Кінцеві статуси платежу
        ['name' => 'error', 'description' => 'Неуспішний платіж. Некоректно заповнені дані'],
        ['name' => 'failure	', 'description' => 'Неуспішний платіж'],
        ['name' => 'reversed', 'description' => 'Платіж повернений'],
        ['name' => 'subscribed', 'description' => 'Підписка успішно оформлена'],
        ['name' => 'success', 'description' => 'Успішний платіж'],
        ['name' => 'unsubscribed', 'description' => 'Підписка успішно деактивована'],
        //Статуси що потребують підтвердження платежу
        ['name' => '3ds_verify', 'description' => 'Потрібна 3DS верифікація. Для завершення платежу, потрібно виконати 3ds_verify'],
        ['name' => 'captcha_verify', 'description' => 'Очікується підтвердження captcha'],
        ['name' => 'cvv_verify', 'description' => 'Потрібне введення CVV картки відправника. Для завершення платежу, потрібно виконати cvv_verify'],
        ['name' => 'ivr_verify', 'description' => 'Очікується підтвердження дзвінком ivr'],
        ['name' => 'otp_verify', 'description' => 'Потрібне OTP підтвердження клієнта. OTP пароль відправлений на номер телефону Клієнта. Для завершення платежу, потрібно виконати otp_verify'],
        ['name' => 'password_verify', 'description' => 'Очікується підтвердження пароля додатка Приват24'],
        ['name' => 'phone_verify', 'description' => 'Очікується введення телефону клієнтом. Для завершення платежу, потрібно виконати phone_verify'],
        ['name' => 'pin_verify', 'description' => 'Очікується підтвердження pin-code'],
        ['name' => 'receiver_verify', 'description' => 'Потрібне введення даних одержувача. Для завершення платежу, потрібно виконати receiver_verify'],
        ['name' => 'sender_verify', 'description' => 'Потрібне введення даних відправника. Для завершення платежу, потрібно виконати sender_verify'],
        ['name' => 'senderapp_verify', 'description' => 'Очікується підтвердження в додатку Privat24'],
        ['name' => 'wait_qr', 'description' => 'Очікується сканування QR-коду клієнтом'],
        ['name' => 'wait_sender', 'description' => 'Очікується підтвердження оплати клієнтом в додатку Privat24/SENDER'],
        //Інші статуси платежу
        ['name' => 'cash_wait', 'description' => 'Очікується оплата готівкою в ТСО'],
        ['name' => 'hold_wait', 'description' => 'Сума успішно заблокована на рахунку відправника'],
        ['name' => 'invoice_wait', 'description' => 'Інвойс створений успішно, очікується оплата'],
        ['name' => 'prepared', 'description' => 'Платіж створений, очікується його завершення відправником'],
        ['name' => 'processing', 'description' => 'Платіж обробляється'],
        ['name' => 'wait_accept', 'description' => 'Кошти з клієнта списані, але магазин ще не пройшов перевірку. Якщо магазин не пройде активацію протягом 90 днів, платежі будуть автоматично скасовані'],
        ['name' => 'wait_card', 'description' => 'Не встановлений спосіб відшкодування у одержувача'],
        ['name' => 'wait_compensation', 'description' => 'Платіж успішний, буде зарахований в щодобовій проводці'],
        ['name' => 'wait_lc', 'description' => 'Акредитив. Кошти з клієнта списані, очікується підтвердження доставки товару'],
        ['name' => 'wait_reserve', 'description' => 'Грошові кошти за платежем зарезервовані для проведення повернення за раніше поданою заявкою'],
        ['name' => 'wait_secure', 'description' => 'Платіж на перевірці'],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $paymentStatuses = $this->getPaymentStatuses();

        foreach ($paymentStatuses as $paymentStatus){
            PaymentStatus::factory()->create([
                'name' => $paymentStatus['name'],
                'description' => $paymentStatus['description'],
            ]);
        }
    }

    /**
     * @return array|array[]
     * @see PaymentStatusSeeder::$paymentStatuses
     */
    public function getPaymentStatuses(): array
    {
        return $this->paymentStatuses;
    }
}
