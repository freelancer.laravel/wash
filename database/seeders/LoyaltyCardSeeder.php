<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\LoyaltyCards\LoyaltyCard;
use Illuminate\Database\Seeder;

/**
 * Class LoyaltyCardSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoyaltyCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        LoyaltyCard::factory(10)->create();
    }
}
