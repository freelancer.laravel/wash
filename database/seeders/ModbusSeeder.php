<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Modbus\Modbus;
use Illuminate\Database\Seeder;

/**
 * Class ModbusSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class ModbusSeeder extends Seeder
{
    /**
     * @var array guarded fields
     */
    protected $guarded = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Modbus::factory(5)->create();
    }
}
