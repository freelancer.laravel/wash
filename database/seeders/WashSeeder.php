<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Washes\Wash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class WashSeeder.
 *
 * @package Database\Seeders
 * @author DaKoshin.
 */
final class WashSeeder extends Seeder
{
    /**
     * @var array|array[] washes
     */
    private array $washes = [
        [
            'latitude' => 49.86,
            'longitude' => 24.05,
            'address' => 'м. Львів Вул. Богдана Хмельницького 200',
            'postcode' => 79000,
        ],
        [
            'latitude' => 49.83,
            'longitude' => 23.96,
            'address' => 'м. Львів Вул. Рудненська 14',
            'postcode' => 79000,
        ],
        [
            'latitude' => 49.86,
            'longitude' => 23.95,
            'address' => 'м. Львів Вул. Шевченка 360Г',
            'postcode' => 79069,
        ],
        [
            'latitude' => 49.27,
            'longitude' => 23.80,
            'address' => 'м. Стрий вул. Промислова 1б',
            'postcode' => 82400,
        ],
        [
            'latitude' => 51.23,
            'longitude' => 24.03,
            'address' => 'м. Любомиль вул. Бреська 4',
            'postcode' => 44300,
        ],
    ];

    /**
     * Run seeder.
     */
    public function run()
    {
        DB::transaction(function () {
            foreach ($this->getWashes() as $wash){
                Wash::factory()->create([
                    'latitude' => $wash['latitude'],
                    'longitude' => $wash['longitude'],
                    'address' => $wash['address'],
                    'postcode' => $wash['postcode'],
                ]);
            }
        });
    }

    /**
     * @return array|array[]
     * @see WashSeeder::$washes
     */
    public function getWashes(): array
    {
        return $this->washes;
    }
}
