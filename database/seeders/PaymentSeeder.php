<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Payments\Payment;
use Illuminate\Database\Seeder;

/**
 * Class PaymentSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Payment::factory(5)->create();
    }
}
