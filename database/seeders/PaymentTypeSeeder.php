<?php
declare(strict_types=1);

namespace Database\Seeders;

use App\Models\PaymentTypes\PaymentType;
use Illuminate\Database\Seeder;

/**
 * Class PaymentTypeSeeder
 *
 * @package Database\Seeders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        PaymentType::factory()->create(['name' => 'cash']);
        PaymentType::factory()->create(['name' => 'paypass']);
        PaymentType::factory()->create(['name' => 'liqpay']);
        PaymentType::factory()->create(['name' => 'privat-pay']);
        PaymentType::factory()->create(['name' => 'google-pay']);
        PaymentType::factory()->create(['name' => 'apple-pay']);
    }
}
