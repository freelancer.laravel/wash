<?php
declare(strict_types=1);

namespace Database\Factories\Payments;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class PaymentFactory
 *
 * @package Database\Factories\Payments
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'wash_id' => 1,
            'name' => 'Liqpay',
            'public_key' => 'sandbox_i79473709509',
            'private_key' => 'sandbox_99AkEmLrYML5OUzOyuNGA7sWU5VUDYYkUN85kPP8',
        ];
    }
}
