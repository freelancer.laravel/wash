<?php
declare(strict_types=1);

namespace Database\Factories\LoyaltyCards;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class LoyaltyCardFactory
 *
 * @package Database\Factories\LoyaltyCards
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoyaltyCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'name' => $this->faker->creditCardType,
            'number' => $this->faker->creditCardNumber,
        ];
    }
}
