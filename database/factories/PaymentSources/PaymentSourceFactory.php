<?php
declare(strict_types=1);

namespace Database\Factories\PaymentSources;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class PaymentSourceFactory
 *
 * @package Database\Factories\PaymentSources
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentSourceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [

        ];
    }
}
