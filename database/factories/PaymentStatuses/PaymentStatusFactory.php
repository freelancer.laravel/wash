<?php
declare(strict_types=1);

namespace Database\Factories\PaymentStatuses;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class PaymentStatusFactory
 *
 * @package Database\Factories\PaymentStatuses
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentStatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            //
        ];
    }
}
