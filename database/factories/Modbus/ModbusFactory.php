<?php
declare(strict_types=1);

namespace Database\Factories\Modbus;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class ModbusFactory
 *
 * @package Database\Factories
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class ModbusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'wash_id' => 1,
            'ip_address' => '192.168.71.2',
        ];
    }
}
