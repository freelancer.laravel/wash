<?php
declare(strict_types=1);

namespace Database\Factories\Orders;

use App\Models\PaymentSources\PaymentSource;
use App\Models\PaymentTargets\PaymentTarget;
use App\Models\PaymentTypes\PaymentType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class OrderFactory
 *
 * @package Database\Factories\Orders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition(): array
    {
        $rand_wash_id = $this->faker->numberBetween(1, 5);

        $payment_type_ids = PaymentType::pluck('id');
        $payment_source_ids = PaymentSource::pluck('id');
        $payment_target_ids = PaymentTarget::pluck('id');
        $payment_target_id = $payment_target_ids[$this->faker->numberBetween(0, count($payment_target_ids) - 1)] ?? 1;
        $payment_target_name = PaymentTarget::where('id', $payment_target_id)->value('name');

        return [
            'user_id' => $this->faker->numberBetween(1, 10),
            'wash_id' => $rand_wash_id,
            'amount' => self::ceilCoefficient(40, 360, 10),
            'payment_type_id' => $payment_type_ids[$this->faker->numberBetween(0, count($payment_type_ids) - 1)] ?? 1,
            'payment_source_id' => $payment_source_ids[$this->faker->numberBetween(0, count($payment_source_ids) - 1)] ?? 1,
            'payment_target_id' => $payment_target_ids[$this->faker->numberBetween(0, count($payment_target_ids) - 1)] ?? 1,
            'payment_result_id_or_value' => $this->getPaymentResultIdOrValue($payment_target_name),
            'payment_status_id' => $this->faker->numberBetween(1, 9),
        ];
    }

    /**
     * Get ceil coefficient
     *
     * @param int $min
     * @param int $max
     * @param int $rate
     * @return float|int
     * @throws \Exception
     */
    public function ceilCoefficient(int $min, int $max, int $rate = 50): float|int
    {
        $number = random_int($min, $max);

        return ceil($number / $rate) * $rate;
    }

    /**
     * GetPaymentResultIdOrValue
     *
     * @param $payment_target_name
     * @return int
     */
    public function getPaymentResultIdOrValue($payment_target_name): int
    {
        $payment_result_id_or_value = 1;

        switch ($payment_target_name){
            case 'box':
                $payment_result_id_or_value =  $this->faker->numberBetween(1, 10);
                break;
            case 'coin':
                $payment_result_id_or_value =  $this->faker->numberBetween(1, 100);
                break;
            case 'loyalty-card':
                $payment_result_id_or_value =  $this->faker->numberBetween(1, 20);
                break;
        }

        return $payment_result_id_or_value;
    }
}
