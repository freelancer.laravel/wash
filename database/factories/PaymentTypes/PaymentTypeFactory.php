<?php
declare(strict_types=1);

namespace Database\Factories\PaymentTypes;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class PaymentTypeFactory
 *
 * @package Database\Factories\PaymentTypes
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentTypeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            //
        ];
    }
}
