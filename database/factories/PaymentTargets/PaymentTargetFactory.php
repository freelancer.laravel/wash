<?php
declare(strict_types=1);

namespace Database\Factories\PaymentTargets;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class PaymentTargetFactory
 *
 * @package Database\Factories\PaymentTargets
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentTargetFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            //
        ];
    }
}
