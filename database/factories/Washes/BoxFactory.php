<?php
declare(strict_types=1);

namespace Database\Factories\Washes;

use App\Models\Washes\Box;
use App\Models\Washes\Wash;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

/**
 * Class BoxFactory.
 *
 * @package Database\Factories\Washes
 * @author DaKoshin.
 */
class BoxFactory extends Factory
{
    /**
     * @var string Model.
     */
    protected $model = Box::class;

    /**
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'wash_id' => Wash::inRandomOrder()->first()->id,
            'number' => random_int(1, 10),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
