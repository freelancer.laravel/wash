<?php
declare(strict_types=1);

namespace Database\Factories\Washes;

use App\Models\Washes\Wash;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class WashFactory.
 *
 * @package Database\Factories\Washes
 * @author DaKoshin.
 */
class WashFactory extends Factory
{
    /**
     * @var string Model.
     */
    protected $model = Wash::class;

    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            //
        ];
    }
}
