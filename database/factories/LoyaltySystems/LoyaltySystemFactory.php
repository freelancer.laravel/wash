<?php
declare(strict_types=1);

namespace Database\Factories\LoyaltySystems;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class LoyaltySystemFactory
 *
 * @package Database\Factories\LoyaltySystems
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoyaltySystemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => 'LoyaltySystem',
            'amount' => '100',
            'percent_from_amount' => '15',
            'note' => 'Ok',
        ];
    }
}
