<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('wash_id');
            $table->unsignedBigInteger('amount');
            $table->unsignedBigInteger('payment_type_id');
            $table->unsignedBigInteger('payment_source_id');
            $table->unsignedBigInteger('payment_target_id');
            $table->unsignedBigInteger('payment_result_id_or_value');
            $table->unsignedBigInteger('payment_status_id')->nullable();
            $table->boolean('is_paid')->default(0);
            $table->foreign('wash_id')->references('id')->on('washes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
