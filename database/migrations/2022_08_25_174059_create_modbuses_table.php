<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('modbuses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('wash_id');
            $table->string('ip_address');
            $table->foreign('wash_id')->references('id')->on('washes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('modbuses');
    }
};
