<?php
declare(strict_types=1);

namespace App\DTO\Factories\Payments\Liqpay;

use App\DTO\Payments\Liqpay\LiqpayDTO;
use Illuminate\Http\Request;

/**
 * Class LiqpayDTOFactory
 *
 * @package App\DTO\Factories\Payments\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LiqpayDTOFactory
{
    /**
     * @param Request $request
     * @return LiqpayDTO
     */
    public static function createFromRequest(Request $request): LiqpayDTO
    {
        return self::createFromArray($request->validated());
    }

    /**
     * @param array $data
     * @return LiqpayDTO
     */
    public static function createFromArray(array $data): LiqpayDTO
    {
        return new LiqpayDTO(
            null,
            (int) $data['wash_id'],
            (int) $data['box_id'],
            (int) $data['amount'],
        );
    }
}
