<?php
declare(strict_types=1);

namespace App\DTO\Factories\Orders;

use App\DTO\Orders\OrderDTO;
use Illuminate\Http\Request;

/**
 * Class OrderDTOFactory
 *
 * @package App\DTO\Factories\Payments\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class OrderDTOFactory
{
    /**
     * @param Request $request
     * @return OrderDTO
     */
    public static function createFromRequest(Request $request): OrderDTO
    {
        return self::createFromArray($request->validated());
    }

    /**
     * @param array $data
     * @return OrderDTO
     */
    public static function createFromArray(array $data): OrderDTO
    {
        return new OrderDTO(
            null,
            (int) $data['wash_id'],
            (int) $data['box_id'],
            (int) $data['amount'],
        );
    }
}
