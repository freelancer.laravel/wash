<?php
declare(strict_types=1);

namespace App\DTO\Payments\Liqpay;

/**
 * Class LiqpayDTO
 *
 * @package App\DTO\Payments\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LiqpayDTO
{
    /**
     * @var int|null Order_id
     */
    private ?int $order_id;

    /**
     * @var int Wash_id
     */
    private int $wash_id;

    /**
     * @var int Box_id
     */
    private int $box_id;

    /**
     * @var int
     */
    private int $amount;

    /**
     * Liq payDTO constructor.
     *
     * @param int|null $order_id
     * @param int $wash_id
     * @param int $box_id
     * @param int $amount
     */
    public function __construct(?int $order_id, int $wash_id, int $box_id, int $amount)
    {
        $this->order_id = $order_id;
        $this->wash_id = $wash_id;
        $this->box_id = $box_id;
        $this->amount = $amount;
    }

    /**
     * @param int $order_id
     * @return void
     */
    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

    /**
     * @return int|null
     * @see LiqpayDTO::$order_id
     */
    public function getOrderId(): int|null
    {
        return $this->order_id;
    }

    /**
     * @return int
     * @see LiqpayDTO::$wash_id
     */
    public function getWashId(): int
    {
        return $this->wash_id;
    }

    /**
     * @return int
     * @see LiqpayDTO::$box_id
     */
    public function getBoxId(): int
    {
        return $this->box_id;
    }

    /**
     * @return int
     * @see LiqpayDTO::$amount
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}
