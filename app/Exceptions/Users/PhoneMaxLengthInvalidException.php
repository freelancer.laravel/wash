<?php
declare(strict_types=1);

namespace App\Exceptions\Users;

use Illuminate\Http\Response;
use InvalidArgumentException;
use Throwable;

/**
 * Class PhoneLengthInvalidException.
 *
 * @package App\Exceptions\Users
 * @author DaKoshin.
 */
final class PhoneMaxLengthInvalidException extends InvalidArgumentException implements Throwable
{
    /**
     * PhoneLengthInvalidException constructor.
     *
     * @param int $length
     */
    public function __construct(int $length)
    {
        $message = __('exceptions.users.phoneMaxLength', ['length' => $length]);

        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
