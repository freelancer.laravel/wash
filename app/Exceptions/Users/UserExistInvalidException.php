<?php
declare(strict_types=1);

namespace App\Exceptions\Users;

use Illuminate\Http\Response;
use InvalidArgumentException;
use Throwable;

/**
 * Class UserExistInvalidException.
 *
 * @package App\Exceptions\Users
 * @author DaKoshin.
 */
final class UserExistInvalidException extends InvalidArgumentException implements Throwable
{
    /**
     * UserExistInvalidException constructor.
     */
    public function __construct()
    {
        $message = __('exceptions.users.userExist');

        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
