<?php
declare(strict_types=1);

namespace App\Exceptions\Users;

use Illuminate\Http\Response;
use InvalidArgumentException;
use Throwable;

/**
 * Class PhoneMinLengthInvalidException.
 *
 * @package App\Exceptions\Users
 * @author DaKoshin.
 */
final class PhoneMinLengthInvalidException extends InvalidArgumentException implements Throwable
{
    /**
     * PhoneMinLengthInvalidException constructor.
     *
     * @param int $length
     */
    public function __construct(int $length)
    {
        $message = __('exceptions.users.phoneMixLength', ['length' => $length]);

        parent::__construct($message, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
