<?php
declare(strict_types=1);

namespace App\Listeners\Payments\Liqpay;

use App\Events\Payments\PaidEvent;
use App\Models\Modbus\Modbus;
use App\Services\Phpmodbus\PhpmodbusService;

/**
 * Class CarWashPaymentNoticeListener
 *
 * @package App\Listeners\Payments\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class CarWashPaymentNoticeListener
{
    /**
     * Handle the event.
     *
     * @param PaidEvent $event
     * @return void
     */
    public function handle(PaidEvent $event): void
    {
        $order = $event->getOrder();
        $wash = $order->wash;
        $modbus = $wash->modbus;
        $ip_address = $modbus->ip_address;
        $box = $order->box;
        $number_box = $box->number;
        $amount = $order->amount;

        app(PhpmodbusService::class)->writeMultipleRegister(
            $ip_address,
            Modbus::UNIT_ID,
            Modbus::REFERENCE,
            $number_box,
            $amount
        );
    }
}
