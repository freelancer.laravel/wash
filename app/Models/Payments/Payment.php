<?php
declare(strict_types=1);

namespace App\Models\Payments;

use App\Models\Washes\Wash;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Payment
 *
 * @package App\Models\Payments
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class Payment extends Model
{
    use HasFactory;

    /**
     * @var array guarded fields
     */
    protected $guarded = [];

    /**
     * Return wash.
     *
     * @return BelongsTo
     */
    public function wash(): BelongsTo
    {
        return $this->belongsTo(Wash::class);
    }
}
