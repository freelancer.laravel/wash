<?php
declare(strict_types=1);

namespace App\Models\PaymentTypes;

use App\Models\Orders\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class PaymentType
 *
 * @package App\Models\PaymentTypes
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentType extends Model
{
    use HasFactory;

    /**
     * @var array guarded fields
     */
    protected $guarded = [];

    /**
     * Get orders
     *
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
