<?php
declare(strict_types=1);

namespace App\Models\PaymentStatuses;

use App\Models\Orders\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class PaymentStatus
 *
 * @package App\Models\PaymentStatuses
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentStatus extends Model
{
    use HasFactory;

    /**
     * @var array guarded fields
     */
    protected $guarded = [];

    /**
     * Get orders
     *
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
