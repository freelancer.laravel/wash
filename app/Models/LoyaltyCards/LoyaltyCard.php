<?php
declare(strict_types=1);

namespace App\Models\LoyaltyCards;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class LoyaltyCard
 *
 * @package App\Models\LoyaltyCards
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoyaltyCard extends Model
{
    use HasFactory;

    /**
     * Return user
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
