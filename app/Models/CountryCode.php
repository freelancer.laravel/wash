<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * CountryCode model.
 *
 * @package App\Models
 * @author DaKoshin.
 */
final class CountryCode extends Model
{
    /**
     * Guarded columns for mass create.
     *
     * @var string[]
     */
    protected $guarded = ['id'];

    /**
     * @var bool Disable using timestamps.
     */
    public $timestamps = false;
}
