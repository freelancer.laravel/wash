<?php
declare(strict_types=1);

namespace App\Models\Modbus;

use App\Models\Washes\Wash;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Modbus
 *
 * @package App\Models
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class Modbus extends Model
{
    use HasFactory;

    /**
     * UNIT_ID
     */
    const UNIT_ID = 255;

    /**
     * REFERENCE
     */
    const REFERENCE = 2000;

    /**
     * QUANTITY REGISTERS
     */
    const QUANTITY = 20;

    /**
     * Return Wash
     *
     * @return BelongsTo
     */
    public function wash(): BelongsTo
    {
        return $this->belongsTo(Wash::class);
    }
}
