<?php
declare(strict_types=1);

namespace App\Models\LoyaltySystems;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LoyaltySystem
 *
 * @package App\Models\LoyaltySystems
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LoyaltySystem extends Model
{
    use HasFactory;
}
