<?php
declare(strict_types=1);

namespace App\Models\PaymentTargets;

use App\Models\Orders\Order;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class PaymentTarget
 *
 * @package App\Models\PaymentTargets
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaymentTarget extends Model
{
    use HasFactory;

    /**
     * @var array guarded fields
     */
    protected $guarded = [];

    /**
     * Get orders
     *
     * @return HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class);
    }
}
