<?php
declare(strict_types=1);

namespace App\Models\Washes;

use App\Models\Modbus\Modbus;
use App\Models\Payments\Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Wash model.
 *
 * @package App\Models\Washes
 * @author DaKoshin.
 */
final class Wash extends Model
{
    use HasFactory;

    /**
     * Guarded columns for mass create.
     *
     * @var string[]
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * Return all wash boxes.
     *
     * @return HasMany|Box[]
     */
    public function boxes(): HasMany|array
    {
        return $this->hasMany(Box::class);
    }

    /**
     * Return modbus
     *
     * @return HasOne
     */
    public function modbus(): HasOne
    {
        return $this->hasOne(Modbus::class);
    }

    /**
     * Return modbus
     *
     * @return HasOne
     */
    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class);
    }
}
