<?php
declare(strict_types=1);

namespace App\Models\Washes;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Box model.
 *
 * @package App\Models\Washes
 * @author DaKoshin.
 */
final class Box extends Model
{
    use HasFactory;

    /**
     * Guarded columns for mass create.
     *
     * @var string[]
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
