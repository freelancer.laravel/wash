<?php
declare(strict_types=1);

namespace App\Models\Orders;

use App\Models\PaymentSources\PaymentSource;
use App\Models\PaymentStatuses\PaymentStatus;
use App\Models\PaymentTargets\PaymentTarget;
use App\Models\PaymentTypes\PaymentType;
use App\Models\Washes\Box;
use App\Models\Washes\Wash;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Order
 *
 * @package App\Models\Orders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class Order extends Model
{
    use HasFactory;

    /**
     * @var array guarded fields
     */
    protected $guarded = [];

    /**
     * Default orders per page
     */
    const PAGINATE = 10;

    /**
     * Return wash.
     *
     * @return BelongsTo
     */
    public function wash(): BelongsTo
    {
        return $this->belongsTo(Wash::class);
    }

    /**
     * Return box.
     *
     * @return BelongsTo
     */
    public function box(): BelongsTo
    {
        return $this->belongsTo(Box::class);
    }

    /**
     * Return payment_type
     *
     * @return BelongsTo
     */
    public function payment_type(): BelongsTo
    {
        return $this->belongsTo(PaymentType::class);
    }

    /**
     * Return payment_source
     *
     * @return BelongsTo
     */
    public function payment_source(): BelongsTo
    {
        return $this->belongsTo(PaymentSource::class);
    }

    /**
     * Return payment_target
     *
     * @return BelongsTo
     */
    public function payment_target(): BelongsTo
    {
        return $this->belongsTo(PaymentTarget::class);
    }

    /**
     * Return payment_status
     *
     * @return BelongsTo
     */
    public function payment_status(): BelongsTo
    {
        return $this->belongsTo(PaymentStatus::class);
    }
}
