<?php
declare(strict_types=1);

namespace App\Services\Phpmodbus;

use App\Libraries\Phpmodbus\ModbusMasterTcp;
use Illuminate\Support\Facades\Log;

/**
 * Class PhpmodbusService
 *
 * @package App\Services\Phpmodbus
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PhpmodbusService
{
    /**
     * Read Multiple Registers
     *
     * @param string $host
     * @param int $unitId
     * @param int $reference
     * @param int $quantity
     * @return array
     */
    public function readMultipleRegisters(string $host, int $unitId, int $reference, int $quantity): array
    {
        // Create Modbus object
        $modbus = new ModbusMasterTcp($host);
        $registers = [];

        try {
            // FC 3
            $recData = $modbus->readMultipleRegisters($unitId, $reference, $quantity);
        }
        catch (\Exception $e) {
            // Print error information if any
            abort(523, 'Origin Is Unreachable.');
        }

        // Print read data
        for($i = 1; $i < count($recData) / 2; $i++) {
            $tempArray = [];
            $temp = ($recData[($i-1) * 2]<<8) + ($recData[(($i-1) * 2) + 1]);

            if($temp > 32767){
                $temp -= 65536;
            }

            $tempArray['reg_' . $i] = $temp;
            $registers[] = $tempArray;
        }

        return $registers;
    }

    /**
     * Write Multiple Register
     *
     * @param string $host
     * @param int $unitId
     * @param int $reference
     * @param int $box
     * @param int|float $amount
     * @return void
     */
    public function writeMultipleRegister(string $host, int $unitId, int $reference, int $box, int|float $amount): void
    {
        // Create Modbus object
        $modbus = new ModbusMasterTcp($host);

        // Data to be writen
        $data = array($amount);
        $dataTypes = array("INT");

        try {
            // FC16
            $modbus->writeMultipleRegister($unitId, $reference + $box - 1, $data, $dataTypes);
        }
        catch (\Exception $e) {
            // Add to Log error information if any
            Log::info('Modbus writeMultipleRegister: ' . $e);
            // Print error information if any
            abort(523, 'Origin Is Unreachable.');
        }
    }
}
