<?php
declare(strict_types=1);

namespace App\Services\Payments\Liqpay;

use App\DTO\Payments\Liqpay\LiqpayDTO;
use App\Libraries\Payments\Liqpay\LiqPay;
use App\Models\Orders\Order;
use App\Models\Washes\Box;
use App\Models\Washes\Wash;
use Illuminate\Support\Facades\Auth;

/**
 * Class LiqpayService
 *
 * @package App\Services\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LiqpayService
{
    /**
     * Main default parameter LANGUAGE
     */
    const LANGUAGE = 'uk';

    /**
     * Main default parameter VERSION
     */
    const VERSION = '3';

    /**
     * Main default parameter ACTION
     */
    const ACTION = 'pay';

    /**
     * Main default parameter CURRENCY
     */
    const CURRENCY = 'UAH';

    /**
     * cnb_form raw data for custom form
     *
     * @param LiqpayDTO $liqpayDTO
     * @return mixed
     * @throws \Exception
     */
    public function cnb_form_raw(LiqpayDTO $liqpayDTO): mixed
    {
        $wash = Wash::where('id', $liqpayDTO->getWashId())->first();
        $address_car_wash = $wash->address;
        $number_box_car_wash = Box::where('id', $liqpayDTO->getBoxId())->value('number');
        $payment = $wash->payment;
        $public_key = $payment->public_key;
        $private_key = $payment->private_key;
        $amount = $liqpayDTO->getAmount();
        $description = 'Адреса : ' . $address_car_wash  . ' ;  Мийний пост #' . $number_box_car_wash;
        $order_id = $liqpayDTO->getOrderId();
        $user = Auth::user();
        $phone = isset($user) ? $user->phone : '';
        $client_domain = config('custom_addition.domains.client');
        $server_domain = config('custom_addition.domains.server');
        $result_url_path = '/thank-you?order=' . $order_id;
        $server_url_path  = '/api/orders/set-paid/' . $order_id;
        $result_url = $client_domain . $result_url_path;
        $server_url = $server_domain . $server_url_path;

        $liqpay = new LiqPay($public_key, $private_key);

        return $liqpay->cnb_form_raw(array(
            'language'       => self::LANGUAGE,
            'version'        => self::VERSION,
            'action'         => self::ACTION,
            'currency'       => self::CURRENCY,
            'amount'         => $amount,
            'description'    => $description,
            'order_id'       => $order_id,
            'phone'          => $phone,
            'result_url'     => $result_url,
            'server_url'     => $server_url,
        ));
    }

    /**
     * get_liqpay_by_order
     *
     * @param Order $order
     * @return LiqPay
     */
    public function get_liqpay_by_order(Order $order): LiqPay
    {
        $wash = $order->wash;
        $payment = $wash->payment;
        $public_key = $payment->public_key;
        $private_key = $payment->private_key;

        return new LiqPay($public_key, $private_key);
    }

    /**
     * get_signature_by_order
     *
     * @param Order $order
     * @param string $data_json
     * @return string
     */
    public function get_signature_by_order(Order $order, string $data_json): string
    {
        $liqpay = $this->get_liqpay_by_order($order);

        return $liqpay->get_signature_from_data($data_json);
    }

    /**
     * decode_params_by_order
     *
     * @param Order $order
     * @param string $data_json
     * @return array|null
     */
    public function decode_params_by_order(Order $order, string $data_json): array|null
    {
        $liqpay = $this->get_liqpay_by_order($order);

        return $liqpay->decode_params($data_json);
    }
}
