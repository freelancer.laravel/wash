<?php
declare(strict_types=1);

namespace App\Services\Orders;

use App\DTO\Orders\OrderDTO;
use App\Events\Payments\PaidEvent;
use App\Models\Orders\Order;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class OrderService
 *
 * @package App\Services\Orders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class OrderService
{
    /**
     * Get listing orders.
     *
     * @param $paginate
     * @return LengthAwarePaginator
     */
    public function index($paginate): LengthAwarePaginator
    {
        return Order::orderBy('created_at', 'desc')->paginate($paginate);
    }

    /**
     * Order to store.
     *
     * @param OrderDTO $orderDTO
     * @return Order
     */
    public function store(OrderDTO $orderDTO): Order
    {
        return Order::create([
            'wash_id' => $orderDTO->getWashId(),
            'box_id' => $orderDTO->getBoxId(),
            'amount' => $orderDTO->getAmount(),
        ]);
    }

    /**
     * Set status paid.
     *
     * @param Order $order
     * @return void
     */
    public function setPaid(Order $order): void
    {
        $order->status = 'success';
        $order->save();

        event(new PaidEvent($order));
    }

    /**
     * Set status.
     *
     * @param Order $order
     * @param string $status
     * @return void
     */
    public function setStatus(Order $order, string $status): void
    {
        if ($status === 'success'){
            $this->setPaid($order);
        }else {
            $order->status = $status;
            $order->save();
        }
    }
}
