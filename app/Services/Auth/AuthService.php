<?php
declare(strict_types=1);

namespace App\Services\Auth;

use App\Models\User;
use App\Services\Users\CountryCodeService;
use App\Validators\CreateUserValidator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\NewAccessToken;

/**
 * Class AuthService.
 *
 * @package App\Services\Auth
 * @author DaKoshin.
 */
final class AuthService
{
    /**
     * @var CountryCodeService CountryCodeService.
     */
    private CountryCodeService $countryCodeService;

    /**
     * AuthService constructor.
     *
     * @param CountryCodeService $countryCodeService
     */
    public function __construct(CountryCodeService $countryCodeService)
    {
        $this->countryCodeService = $countryCodeService;
    }

    /**
     * Authorize user by credentials.
     *
     * @param int $countryCodeId
     * @param int $phone
     * @param string $password
     * @param string $deviceName
     * @return NewAccessToken
     */
    public function authenticate(int $countryCodeId, int $phone, string $password, string $deviceName): NewAccessToken
    {
        $countryCode = $this->countryCodeService->getById($countryCodeId);
        $phone = $countryCode->code . $phone;
        $user = User::wherePhone($phone)->first();

        if (!$user || !Hash::check($password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        CreateUserValidator::validateDeviceName($deviceName);

        return $user->createToken($deviceName);
    }

    /**
     * Authorize user.
     *
     * @param User $user
     * @param string $deviceName
     * @return NewAccessToken
     */
    public function authenticateUser(User $user, string $deviceName): NewAccessToken
    {
        $user->tokens()->delete();
        $token = $user->createToken($deviceName);

        Auth::login($user);

        return $token;
    }
}
