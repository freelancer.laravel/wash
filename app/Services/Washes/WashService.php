<?php
declare(strict_types=1);

namespace App\Services\Washes;

use App\Models\Washes\Box;
use App\Models\Washes\Wash;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class WashService.
 *
 * @package App\Services\Washes
 * @author DaKoshin.
 */
final class WashService
{
    /**
     * Return all washes.
     *
     * @return Collection|Box[]
     */
    public function getAll(): Collection|array
    {
        return Wash::get();
    }
}
