<?php
declare(strict_types=1);

namespace App\Services\Washes;

use App\Models\Washes\Box;
use App\Models\Washes\Wash;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BoxService.
 *
 * @package App\Services\Washes
 * @author DaKoshin.
 */
final class BoxService
{
    /**
     * @param Wash $wash
     * @return Collection|Box[]
     */
    public function getAll(Wash $wash): Collection|array
    {
        return $wash->boxes()->get();
    }
}
