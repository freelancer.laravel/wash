<?php
declare(strict_types=1);

namespace App\Services\Users;

use App\Models\CountryCode;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class CountryCodeService.
 *
 * @package App\Services\Users
 * @author DaKoshin.
 */
final class CountryCodeService
{
    /**
     * Return country code by id.
     *
     * @param int $id
     * @return CountryCode
     */
    public function getById(int $id): CountryCode
    {
        return CountryCode::findOrFail($id);
    }

    /**
     * Return all country codes.
     *
     * @return Collection|CountryCode[]
     */
    public function getAllCountryCodes(): Collection|array
    {
        return CountryCode::get();
    }
}
