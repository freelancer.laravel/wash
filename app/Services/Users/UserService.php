<?php
declare(strict_types=1);

namespace App\Services\Users;

use App\Models\User;
use App\Validators\CreateUserValidator;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserService.
 *
 * @package App\Services\Users
 * @author DaKoshin.
 */
final class UserService
{
    /**
     * @var CountryCodeService CountryCodeService.
     */
    private CountryCodeService $countryCodeService;

    /**
     * UserService constructor.
     *
     * @param CountryCodeService $countryCodeService
     */
    public function __construct(CountryCodeService $countryCodeService)
    {
        $this->countryCodeService = $countryCodeService;
    }

    /**
     * Create user.
     *
     * @param int $countryCodeId
     * @param int $phone
     * @param string $password
     * @return User
     */
    public function create(int $countryCodeId, int $phone, string $password): User
    {
        $countryCode = $this->countryCodeService->getById($countryCodeId);
        $user = new User();
        $user->password = Hash::make($password);
        $user->phone = (string) $phone;

        CreateUserValidator::validate($user);

        $user->phone = $countryCode->code . $phone;

        CreateUserValidator::validateExistUser($user);

        $user->save();

        return $user;
    }
}
