<?php
declare(strict_types=1);

namespace App\Validators;

use App\Exceptions\Users\DeviceNameMaxLengthInvalidException;
use App\Exceptions\Users\PhoneMaxLengthInvalidException;
use App\Exceptions\Users\PhoneMinLengthInvalidException;
use App\Exceptions\Users\UserExistInvalidException;
use App\Models\User;

/**
 * Class CreateUserValidator.
 *
 * @package App\Validators
 * @author DaKoshin.
 */
final class CreateUserValidator
{
    /**
     * @var int Min and Max phone length for auth.
     */
    const MIN_MAX_LENGTH_PHONE_NUMBER = 9;

    /**
     * @var int Max password length.
     */
    const MAX_LENGTH_PASSWORD = 32;

    /**
     * @var int Min password length.
     */
    const MIN_LENGTH_PASSWORD = 8;

    /**
     * @var int Max device name length.
     */
    const MAX_LENGTH_DEVICE_NAME = 255;

    /**
     * Validate user phone number.
     *
     * @param string $number
     * @return void
     */
    public static function validatePhone(string $number): void
    {
        $length = strlen($number);

        if ($length > self::MIN_MAX_LENGTH_PHONE_NUMBER) {
            throw new PhoneMaxLengthInvalidException(self::MIN_MAX_LENGTH_PHONE_NUMBER);
        }

        if ($length < self::MIN_MAX_LENGTH_PHONE_NUMBER) {
            throw new PhoneMinLengthInvalidException(self::MIN_MAX_LENGTH_PHONE_NUMBER);
        }
    }

    /**
     * Validate device name.
     *
     * @param string $deviceName
     */
    public static function validateDeviceName(string $deviceName): void
    {
        if (strlen($deviceName) > self::MAX_LENGTH_DEVICE_NAME) {
            throw new DeviceNameMaxLengthInvalidException(self::MAX_LENGTH_DEVICE_NAME);
        }
    }

    /**
     * Validate exist user.
     *
     * @param User $user
     */
    public static function validateExistUser(User $user): void
    {
        $userIsset = User::wherePhone($user->phone)->count();

        if ($userIsset) {
            throw new UserExistInvalidException;
        }
    }

    /**
     * Validate user.
     *
     * @param User $user
     */
    public static function validate(User $user): void
    {
        self::validatePhone($user->phone);
    }
}
