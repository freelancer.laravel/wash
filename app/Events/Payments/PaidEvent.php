<?php
declare(strict_types=1);

namespace App\Events\Payments;

use App\Models\Orders\Order;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * Class PaidEvent
 *
 * @package App\Events\Payments
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class PaidEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Order Order
     */
    private Order $order;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     * @see PaidEvent::$order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }
}
