<?php
declare(strict_types=1);

namespace App\Http\Requests\Auth;

use App\Validators\CreateUserValidator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AuthenticateRequest.
 *
 * @package App\Http\Requests\Auth
 * @author DaKoshin.
 */
final class AuthenticateRequest extends FormRequest
{
    /**
     * Validation rules.
     *
     * @return string[]
     */
    public function rules(): array
    {
        $minMaxLenPhone = CreateUserValidator::MIN_MAX_LENGTH_PHONE_NUMBER;

        return [
            'codeId' => [
                'bail',
                'required',
                'numeric',
                'integer',
                'exists:country_codes,id',
            ],
            'phone' => [
                'bail',
                'required',
                'numeric',
                'integer',
                'digits_between:' . $minMaxLenPhone . ',' . $minMaxLenPhone,
            ],
            'password' => [
                'bail',
                'required',
                'string',
                'min:' . CreateUserValidator::MIN_LENGTH_PASSWORD,
                'max:' . CreateUserValidator::MAX_LENGTH_PASSWORD,
            ],
            'deviceName' => [
                'bail',
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
