<?php
declare(strict_types=1);

namespace App\Http\Requests\Payments\Liqpay;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LiqpayRequest
 *
 * @package App\Http\Requests\Payments\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LiqpayRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'wash_id' => ['bail', 'required', 'integer'],
            'box_id' => ['bail', 'required', 'integer'],
            'amount' => ['bail', 'required', 'numeric'],
        ];
    }
}
