<?php
declare(strict_types=1);

namespace App\Http\Requests\Users;

use App\Validators\CreateUserValidator;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateUserRequest.
 *
 * @package App\Http\Requests\Users
 * @author DaKoshin.
 */
final class CreateUserRequest extends FormRequest
{
    /**
     * Validation rules.
     *
     * @return string[]
     */
    public function rules(): array
    {
        $minLenPhone = CreateUserValidator::MIN_MAX_LENGTH_PHONE_NUMBER;
        $maxLenPhone = CreateUserValidator::MIN_MAX_LENGTH_PHONE_NUMBER;

        return [
            'codeId' => [
                'bail',
                'required',
                'numeric',
                'integer',
                'exists:country_codes,id',
            ],
            'phone' => [
                'bail',
                'required',
                'numeric',
                'integer',
                'digits_between:' . $minLenPhone . ',' . $maxLenPhone,
            ],
            'password' => [
                'bail',
                'required',
                'string',
                'min:' . CreateUserValidator::MIN_LENGTH_PASSWORD,
                'max:' . CreateUserValidator::MAX_LENGTH_PASSWORD,
                'confirmed',
            ],
            'deviceName' => [
                'bail',
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
