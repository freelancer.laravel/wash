<?php
declare(strict_types=1);

namespace App\Http\Resources;

use App\Models\CountryCode;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CountryCodeResource.
 *
 * @package App\Http\Resources
 * @author DaKoshin.
 * @mixin CountryCode
 */
class CountryCodeResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request = null): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
        ];
    }
}
