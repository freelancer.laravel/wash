<?php
declare(strict_types=1);

namespace App\Http\Resources\Washes;

use App\Models\Washes\Wash;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class WashResource.
 *
 * @mixin Wash
 */
final class WashResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request = null)
    {
        return [
            'id' => $this->id,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'address' => $this->address,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
        ];
    }
}
