<?php
declare(strict_types=1);

namespace App\Http\Resources\Washes;

use App\Models\Washes\Box;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BoxResource.
 *
 * @package App\Http\Resources\Washes
 * @author DaKoshin.
 * @mixin Box
 */
final class BoxResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
        ];
    }
}
