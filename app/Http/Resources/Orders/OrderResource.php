<?php
declare(strict_types=1);

namespace App\Http\Resources\Orders;

use App\Http\Resources\Washes\BoxResource;
use App\Http\Resources\Washes\WashResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class OrderResource
 *
 * @package App\Http\Resources\Orders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request = null): array
    {
        return [
            'id' => $this->id,
            'wash' => new WashResource($this->wash),
            'box' => new BoxResource($this->box),
            'amount' => $this->amount,
            'is_paid' => $this->is_paid,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
        ];
    }
}
