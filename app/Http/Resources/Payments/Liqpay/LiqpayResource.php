<?php
declare(strict_types=1);

namespace App\Http\Resources\Payments\Liqpay;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class LiqpayResource
 *
 * @package App\Http\Resources\Payments\Liqpay
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LiqpayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request = null): array
    {
        return [
            'url' => $this->url,
            'data' => $this->data,
            'signature' => $this->signature,
        ];
    }
}
