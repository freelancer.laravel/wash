<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as Code;

/**
 * Base Controller.
 *
 * @package App\Http\Controllers
 * @author DaKoshin.
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Successful API response.
     *
     * @param string|null $message
     * @param array $data
     * @param int $code
     * @return JsonResponse
     */
    protected function successResponse(
        ?string $message = null,
        array $data = [],
        int $code = Response::HTTP_OK
    ): JsonResponse {
        $result = array_merge(
            $message ? ['message' => __($message)] : [],
            $data
        );

        return response()->json($result, $code);
    }

    /**
     * Successful response with data.
     *
     * @param array $data
     * @param array $additional
     * @return JsonResponse
     */
    protected function dataResponse(array $data = [], array $additional = []): JsonResponse
    {
        return self::successResponse(
            null,
            array_merge(isset($data['data']) ? $data : ['data' => $data], $additional)
        );
    }

    /**
     * Successful API response with no content.
     *
     * @return JsonResponse
     */
    protected function successResponseWithoutContent(): JsonResponse
    {
        return self::successResponse(null, [], Response::HTTP_NO_CONTENT);
    }

    /**
     * API response with an error.
     *
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    protected function errorResponse(string $message, int $code = Response::HTTP_NOT_FOUND): JsonResponse
    {
        return response()->json(['message' => __($message)], $code ?: Code::HTTP_BAD_GATEWAY);
    }
}
