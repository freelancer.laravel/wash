<?php
declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\DTO\SuccessAuthDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Resources\Auth\SuccessAuthResource;
use App\Services\Auth\AuthService;
use App\Services\Users\UserService;
use App\Validators\CreateUserValidator;
use Illuminate\Http\JsonResponse;

/**
 * Class RegisterController.
 *
 * @package App\Http\Controllers\Auth
 * @author DaKoshin.
 */
final class RegisterController extends Controller
{
    /**
     * Register new user.
     *
     * @param CreateUserRequest $request
     * @param UserService $userService
     * @param AuthService $authService
     * @return JsonResponse
     */
    public function register(
        CreateUserRequest $request,
        UserService $userService,
        AuthService $authService
    ): JsonResponse
    {
        CreateUserValidator::validateDeviceName($request->input('deviceName'));

        $user = $userService->create(
            (int)$request->input('codeId'),
            (int)$request->input('phone'),
            $request->input('password')
        );

        $token = $authService->authenticateUser($user, $request->input('deviceName'));

        $resource = new SuccessAuthResource(
            new SuccessAuthDTO($token->accessToken->id, $token->plainTextToken)
        );

        return $this->dataResponse($resource->toArray());
    }
}
