<?php
declare(strict_types=1);

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Resources\CountryCodeResource;
use App\Http\Resources\Users\UserResource;
use App\Services\Users\CountryCodeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

/**
 * Class UsersController.
 *
 * @package App\Http\Controllers\Users
 * @author DaKoshin.
 */
final class UsersController extends Controller
{
    /**
     * Return auth user info.
     *
     * @return JsonResponse
     */
    public function getMe(): JsonResponse
    {
        return $this->dataResponse((new UserResource(Auth::user()))->toArray());
    }

    /**
     * Return all country codes.
     *
     * @param CountryCodeService $countryCodeService
     * @return JsonResponse
     */
    public function getPhoneCountryCodes(CountryCodeService $countryCodeService): JsonResponse
    {
        return $this->dataResponse(
            CountryCodeResource::collection($countryCodeService->getAllCountryCodes())->toArray(request())
        );
    }
}
