<?php
declare(strict_types=1);

namespace App\Http\Controllers\Payments\Liqpay;

use App\DTO\Factories\Orders\OrderDTOFactory;
use App\DTO\Factories\Payments\Liqpay\LiqpayDTOFactory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Payments\Liqpay\LiqpayRequest;
use App\Http\Resources\Payments\Liqpay\LiqpayResource;
use App\Services\Orders\OrderService;
use App\Services\Payments\Liqpay\LiqpayService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

/**
 * Class LiqpayController
 *
 * @package App\Http\Controllers\Payments
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class LiqpayController extends Controller
{
    /**
     * cnb_form raw data for custom form
     *
     * @param LiqpayRequest $request
     * @return JsonResponse
     */
    public function cnb_form_raw(LiqpayRequest $request): JsonResponse
    {
        try {
            //Create new order.
            $orderDTO = app(OrderDTOFactory::class)->createFromRequest($request);
            $order = app(OrderService::class)->store($orderDTO);

            //Generate liqpay raw data for custom form.
            $liqpayDTO = app(LiqpayDTOFactory::class)->createFromRequest($request);
            $liqpayDTO->setOrderId($order->id);
            $cnb_form_raw = (object) app(LiqpayService::class)->cnb_form_raw($liqpayDTO);
        }catch (\Exception $e){
            // Add to Log error information if any
            Log::info('Liqpay cnb_form_raw: ' . $e);

            return $this->errorResponse('Payment is temporarily unavailable.');
        }

        return $this->dataResponse((new LiqpayResource($cnb_form_raw))->toArray(request()));
    }
}
