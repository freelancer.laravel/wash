<?php
declare(strict_types=1);

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Http\Resources\Orders\OrderResource;
use App\Models\Orders\Order;
use App\Services\Orders\OrderService;
use App\Services\Payments\Liqpay\LiqpayService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class OrderController
 *
 * @package App\Http\Controllers\Orders
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginate = $request->paginate ?? Order::PAGINATE;
        $orders = app(OrderService::class)->index($paginate);

        return $this->dataResponse((OrderResource::collection($orders))->toArray(request()));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $order = Order::findOrFail($id);

        return $this->dataResponse((new OrderResource($order))->toArray());
    }

    /**
     * Set status payment
     *
     * @param Request $request
     * @param int $order_id
     * @return JsonResponse
     */
    public function setStatus(Request $request, int $order_id): JsonResponse
    {
        try {
            $order = Order::findOrFail($order_id);
            $data_json = $request->data;

            if (!$data_json){
                abort(400);
            }

            $data = app(LiqpayService::class)->decode_params_by_order($order, $data_json);

            app(OrderService::class)->setStatus($order, $data['status']);
        }catch (\Exception $e){
            return $this->errorResponse('Error set status.');
        }

        return $this->successResponseWithoutContent();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return $this->successResponseWithoutContent();
    }
}
