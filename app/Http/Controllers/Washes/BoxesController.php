<?php
declare(strict_types=1);

namespace App\Http\Controllers\Washes;

use App\Http\Controllers\Controller;
use App\Http\Resources\Washes\BoxResource;
use App\Models\Washes\Wash;
use App\Services\Washes\BoxService;
use Illuminate\Http\JsonResponse;

/**
 * Class BoxesController.
 *
 * @package App\Http\Controllers\Washes
 * @author DaKoshin.
 */
class BoxesController extends Controller
{
    /**
     * Return all wash boxes.
     *
     * @param Wash $wash
     * @param BoxService $boxService
     * @return JsonResponse
     */
    public function __invoke(Wash $wash, BoxService $boxService): JsonResponse
    {
        return $this->dataResponse(BoxResource::collection($boxService->getAll($wash))->toArray(request()));
    }
}
