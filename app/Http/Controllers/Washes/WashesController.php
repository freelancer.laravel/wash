<?php
declare(strict_types=1);

namespace App\Http\Controllers\Washes;

use App\Http\Controllers\Controller;
use App\Http\Resources\Washes\WashResource;
use App\Services\Washes\WashService;
use Illuminate\Http\JsonResponse;

/**
 * Class WashesController.
 *
 * @package App\Http\Controllers\Washes
 * @author DaKoshin.
 */
final class WashesController extends Controller
{
    /**
     * Return all washes.
     *
     * @param WashService $washService
     * @return JsonResponse
     */
    public function __invoke(WashService $washService): JsonResponse
    {
        $washes = $washService->getAll();

        return $this->dataResponse(WashResource::collection($washes)->toArray(request()));
    }
}
