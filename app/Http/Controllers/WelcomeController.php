<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Response;

/**
 * Class WelcomeController.
 *
 * @package App\Http\Controllers
 * @author DaKoshin.
 */
class WelcomeController extends Controller
{
    /**
     * Abort main page only api.
     */
    public function __invoke()
    {
        abort(Response::HTTP_NOT_FOUND);
    }
}
