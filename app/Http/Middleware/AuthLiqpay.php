<?php
declare(strict_types=1);

namespace App\Http\Middleware;

use App\Models\Orders\Order;
use App\Services\Payments\Liqpay\LiqpayService;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class IsPaid
 *
 * @package App\Http\Middleware
 * @author <freelancer.laravel.vue@gmail.com>
 */
final class AuthLiqpay
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse
    {
        try {
            $order_id = $request->segment(4);
            $data_json = $request->data;
            $signatureFromRequest = $request->signature;

            if (!$data_json){
                abort(400);
            }

            $order = Order::findOrFail($order_id);
            $signatureServer = app(LiqpayService::class)->get_signature_by_order($order, $data_json);

            //Authentication of the response from the LiqPay server
            if ($signatureServer == $signatureFromRequest){
                return $next($request);
            }
        }catch (\Exception $e){
            abort(401,'Unauthorized');
        }

        abort(401,'Unauthorized');
    }
}
