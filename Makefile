up:
	./sail up -d
down:
	./sail down
db-refresh:
	./sail artisan migrate:refresh --seed
bash:
	./sail exec samwash.app bash
composer-dump:
	./sail composer dump
clear-cache:
	./sail artisan optimize:clear
