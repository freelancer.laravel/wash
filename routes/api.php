<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Orders\OrderController;
use App\Http\Controllers\Payments\Liqpay\LiqpayController;
use App\Http\Controllers\Users\UsersController;
use App\Http\Controllers\Washes\BoxesController;
use App\Http\Controllers\Washes\WashesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication routes
Route::prefix('auth')->group(function () {
    Route::post('/login', [LoginController::class, 'authenticate']);
    Route::post('/register', [RegisterController::class, 'register']);
    Route::get('/country-codes', [UsersController::class, 'getPhoneCountryCodes']);
    Route::post('/logout', [LoginController::class, 'logout'])->middleware('auth:sanctum');
    Route::post('/logout-all', [LoginController::class, 'logoutFromAll'])->middleware('auth:sanctum');
});

// Washes routes.
Route::prefix('washes')->group(function () {
    Route::get('/', WashesController::class);

    //Boxes
    Route::get('/{wash}/boxes', BoxesController::class);
});

// Auth protected routes.
Route::middleware('auth:sanctum')->group(function () {
    // Users.
    Route::prefix('users')->group(function () {
        Route::get('/me', [UsersController::class, 'getMe']);
    });
});

// Payments routes
Route::prefix('payments')->group(function () {
    Route::prefix('liqpay')->group(function () {
        Route::post('cnb-form-raw', [LiqpayController::class, 'cnb_form_raw']);
    });
});

// Orders routes
Route::prefix('orders')->group(function () {
    Route::post('/set-status/{order_id}', [OrderController::class, 'setStatus'])->middleware('auth.liqpay');
    Route::get('/show/{id}', [OrderController::class, 'show']);
    Route::post('/delete/{id}', [OrderController::class, 'destroy']);
    Route::get('/', [OrderController::class, 'index']);
});
